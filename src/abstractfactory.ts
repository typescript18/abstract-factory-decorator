import { concreteFactory2 } from "./Classes/AbstractFactory/Factories/concreteFactory2";
import { concreteFactoryIObeya } from "./Classes/AbstractFactory/Factories/concreteFactoryIObeya";
import { AbstractFactory } from "./Classes/AbstractFactory/Workers/AbstractFactory";

    function clientCode(factory: AbstractFactory) {
        const workerA = factory.createWorkerOne();
        const workerB = factory.createWorkerTwo();
        const workerC = factory.createWorkerThree();
        console.log("\n=> Worker A");
        console.log(workerA.method1());
        console.log(workerA.method2());
        console.log(workerA.method3());
        console.log(workerA.method4());

        console.log("\n=> Worker B");
        console.log(workerB.method1());
        console.log(workerB.method2());
        console.log(workerB.method3());
        console.log(workerB.method4());

        console.log("\n=> Worker C");
        console.log(workerC.method1());
        console.log(workerC.method2());
        console.log(workerC.method3());
        console.log(workerC.method4());
    }

    console.log('Testing client code with the factory type IObeya...');
    //logs out : 
    //The result of the worker TPA - meth 1.
    //The result of the GenericWorkerA - meth 2.
    //The result of the worker TPA - meth 3.
    //The result of the GenericWorkerA - meth 4.

    //The result of the worker C130 - meth 1.
    //The result of the GenericWorkerA - meth 2.
    //The result of the worker C130 - meth 3.
    //The result of the GenericWorkerA - meth 4.

    //meth 1 - void !
    //The result of the GenericWorkerA - meth 2.
    //meth 3 - void !
    //The result of the GenericWorkerA - meth 4.
    var factory = new concreteFactoryIObeya();
    clientCode(factory);

    console.log('testing factory 2');

    console.log('Client: Testing client code with the factory type 2...');
    //logs out : 
    //The result of the worker B1 - meth 1.
    //The result of the worker B1 - meth 3.
    //meth 2 - void !
    //meth 4 - void !

    //The result of the worker B2 - meth 2.
    //meth 4 - void !
    //meth 1 - void !
    //The result of the worker B2 - meth 3.

    //throws Error('Not implemented');
    var factory = new concreteFactory2();
    clientCode(factory);

