import { C130Builder } from "./Classes/Builder/ConcreteBuilders/C130Builder";
import { GenericBuilder } from "./Classes/Builder/ConcreteBuilders/GenericBuilder";
import { TPABuilder } from "./Classes/Builder/ConcreteBuilders/TPABuilder";
import { C130Director } from "./Classes/Builder/Directors/C130Director";
import { GenericDirector } from "./Classes/Builder/Directors/GenericDirector";
import { TPADirector } from "./Classes/Builder/Directors/TPADirector";

function clientCode(director: GenericDirector) {
    console.log('Default');
    director.doProcess();
    director.getResults().listParts();

    console.log('Specific');
    director.doSpecificProcess();
    director.getResults().listParts();
}

var builder ;
builder = new GenericBuilder();
var director = new GenericDirector(builder);
clientCode(director);


builder = new C130Builder();
director = new C130Director(builder);
clientCode(director);

builder = new TPABuilder();
director = new TPADirector(builder);
clientCode(director);