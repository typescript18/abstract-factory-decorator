import { AbstractFactory, AbstractWorkerIObeya, ConcreteWorkerC130, ConcreteWorkerTPA, GenericWorkerA } from "../Workers/AbstractFactory";

    export class concreteFactoryIObeya implements AbstractFactory {
        createWorkerOne(): AbstractWorkerIObeya {
            return new ConcreteWorkerTPA();
        }

        createWorkerTwo(): AbstractWorkerIObeya {
            return new ConcreteWorkerC130();
        }

        createWorkerThree(): AbstractWorkerIObeya {
            return new GenericWorkerA();
        }
    }