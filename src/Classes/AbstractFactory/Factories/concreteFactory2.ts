import { AbstractFactory, AbstractWorkerIObeya, ConcreteworkerB1, ConcreteworkerB2 } from "../Workers/AbstractFactory";

    export class concreteFactory2 implements AbstractFactory {
        createWorkerOne(): AbstractWorkerIObeya {
            return new ConcreteworkerB1();
        }

        createWorkerTwo(): AbstractWorkerIObeya {
            return new ConcreteworkerB2();
        }

         createWorkerThree(): AbstractWorkerIObeya {
            console.log('not implemented');
            throw new Error('Not implemented');
        }
    }