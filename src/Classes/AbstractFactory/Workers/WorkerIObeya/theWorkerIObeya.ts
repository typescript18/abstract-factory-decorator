import { AbstractFactory, AbstractWorkerIObeya } from "../AbstractFactory";

    export class GenericWorkerA implements AbstractWorkerIObeya{
        method2(): string {
            return 'The result of the GenericWorkerA - meth 2.';
        }

        method4(): string {
            return 'The result of the GenericWorkerA - meth 4';
        }

        method1() : string {
            return 'meth 1 - void !';
        }
        method3() : string {
            return 'meth 3 - void !';
        }

    }

    export class ConcreteWorkerTPA extends GenericWorkerA {
        method1(): string {
            return 'The result of the worker TPA - meth 1.';
        }
        method3(): string {
            return 'The result of the worker TPA - meth 3.';
        }
    }

    export class ConcreteWorkerC130 extends GenericWorkerA {
        method1(): string {
            return 'The result of the worker C130 - meth 1.';
        }
        method3(): string {
            return 'The result of the worker C130 - meth 3.';
        }
    }
