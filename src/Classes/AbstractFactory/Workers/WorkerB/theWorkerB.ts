import { AbstractWorkerIObeya } from "../AbstractFactory";

    export class ConcreteworkerB1 implements AbstractWorkerIObeya {
        method1(): string {
            return 'The result of the worker B1 - meth 1.';
        }

        method4(): string {
            return 'The result of the worker B1 - meth 4.';
        }

        method2() : string {
            return 'meth 2 - void !';
        }
        method3() : string {
            return 'meth 3 - void !';
        }
    }

    export class ConcreteworkerB2 implements AbstractWorkerIObeya {
        method2(): string {
            return 'The result of the worker B2 - meth 2.';
        }

        method3(): string {
            return 'The result of the worker B2 - meth 3.';
        }

        method1() : string {
            return 'meth 1 - void !';
        }
        method4() : string {
            return 'meth 4 - void !';
        }
    }
