export * from './AbstractFactory';
export * from './WorkerIObeya/theWorkerIObeya';
export * from './WorkerB/theWorkerB';

/**
 * The Abstract Factory Pattern
 */
export interface AbstractFactory {
    createWorkerOne(): AbstractWorkerIObeya;

    createWorkerTwo(): AbstractWorkerIObeya;

    createWorkerThree(): AbstractWorkerIObeya;
}

export interface AbstractWorkerIObeya {
    method1() : string;
    method2() : string;
    method3() : string;
    method4() : string;
}
