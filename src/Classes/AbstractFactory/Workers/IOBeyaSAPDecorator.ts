/**
 * The Decorator Pattern
 */
 interface IOBeyaSAPComponent {
    doTheJobPlease() : boolean;
}

export class IOBeyaSAPDecorator implements IOBeyaSAPComponent {
    protected component: IOBeyaSAPComponent;

    constructor(component: IOBeyaSAPComponent) {
        this.component = component;
    }

    public doTheJobPlease() {
        return this.component.doTheJobPlease();
    }
}