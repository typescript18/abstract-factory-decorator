import { GenericBuilder } from "./GenericBuilder";

export class C130Builder extends GenericBuilder {
    produceSpecificC130(): void {
        this.result.add('C130');
    }
}