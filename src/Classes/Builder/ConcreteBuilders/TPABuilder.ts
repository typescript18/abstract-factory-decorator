import { GenericBuilder } from "./GenericBuilder";

export class TPABuilder extends GenericBuilder {
    produceSpecificTPA(): void {
        this.result.add('TPA');
    }
}