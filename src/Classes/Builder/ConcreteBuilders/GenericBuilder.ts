import { IBuilder } from "../IBuilder";
import { IProduct } from "../IProduct";
import { Product1 } from "../Products/Product1";

export class GenericBuilder implements IBuilder{
    protected result: Product1;

    public constructor() {
        this.result = new Product1();
    }

    reset(): void {
        this.result = new Product1();
    }

    producePartA(): void {
        this.result.add('job A');
    }

    producePartB(): void {
        this.result.add('job B');
    }

    producePartC(): void {
        this.result.add('job C');
    }

    getResult() : IProduct {
        const result = this.result;
        this.reset()
        return result;
    }
}
