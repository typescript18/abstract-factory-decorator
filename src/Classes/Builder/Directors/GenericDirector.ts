import { IBuilder } from "../IBuilder";
import { IDirector } from "../IDirector";
import { IProduct } from "../IProduct";

export class GenericDirector implements IDirector {
    protected builder: IBuilder;

    constructor(builder: IBuilder) {
        this.builder = builder;
    }

    public getResults(): IProduct {
        return this.builder.getResult();
    }

    public doProcess() {
        this.builder.producePartA();
        this.builder.producePartB();
        this.builder.producePartC();
    }

    public doSpecificProcess(): void {
        // nothing, generic here !
    }
}