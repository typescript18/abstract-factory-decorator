import { TPABuilder } from "../ConcreteBuilders/TPABuilder";
import { IBuilder } from "../IBuilder";
import { IDirector } from "../IDirector";
import { GenericDirector } from "./GenericDirector";

export class TPADirector extends GenericDirector {

    constructor(builder: TPABuilder) {
        super(builder);
    }

    public doProcess() {
        super.doProcess();
    }

    public doSpecificProcess() {
        this.builder.producePartA();
        this.builder.producePartB();
        this.builder.producePartC();
        (this.builder as TPABuilder).produceSpecificTPA();
    }

}