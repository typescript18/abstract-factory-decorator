import { C130Builder } from "../ConcreteBuilders/C130Builder";
import { GenericDirector } from "./GenericDirector";

export class C130Director extends GenericDirector {

    constructor(builder: C130Builder) {
        super(builder);
    }


    public doProcess() {
        super.doProcess();
    }

    public doSpecificProcess() {
        this.builder.producePartA();
        this.builder.producePartB();
        this.builder.producePartC();
        (this.builder as C130Builder).produceSpecificC130();
    }
}