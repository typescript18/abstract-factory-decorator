import { IProduct } from "../IProduct";

export class Product1 implements IProduct {
    private parts: string[] = [];

    public listParts(): void {
        console.log("Parties du produit : "+this.parts.join(', ')+"\n")
    }

    add(whatToAdd: string): void {
        this.parts.push(whatToAdd);
    }
}