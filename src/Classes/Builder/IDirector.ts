export interface IDirector {
    doProcess(): void;

    doSpecificProcess(): void;
    
    getResults(): void;
}