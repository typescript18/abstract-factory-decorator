import { IProduct } from "./IProduct";

export interface IBuilder {
    reset(): void;
    producePartA(): void;
    producePartB(): void;
    producePartC(): void;
    getResult(): IProduct;
}